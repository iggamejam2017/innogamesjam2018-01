﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTestScript : MonoBehaviour {

    Rigidbody2D rb2d;
    public ParticleSystem JumpParticles;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Jump"))
        {
            rb2d.AddForce(Vector2.up * 10f, ForceMode2D.Impulse);
            JumpParticles.Play();
        }
	}
}
