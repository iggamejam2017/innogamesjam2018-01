﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectInOval : MonoBehaviour {

    public float RX;
    public float RY;

    [Range(0, Mathf.PI * 2)]
    public float StartPi;

    public float Speed;
    private float angle;
    private Vector3 Pos;

	// Use this for initialization
	void Start () {
        angle = StartPi;
        Pos = transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        angle += Speed * Time.deltaTime;
        var x = Pos.x + Mathf.Cos(angle) * RX;
        var y = Pos.y + Mathf.Sin(angle) * RY;
        transform.position = new Vector3(x, y);
    }

    void OnDrawGizmos()
    {
        var p = Pos;
        if (p == Vector3.zero)
            p = transform.position;

        Gizmos.color = Color.red;
        float lastx, lasty;
        lastx = p.x + Mathf.Cos(0) * RX;
        lasty = p.y + Mathf.Sin(0) * RY;
        for (float i = 0; i < 2 * Mathf.PI; i += 0.1f)
        {
            var x = p.x + Mathf.Cos(i) * RX;
            var y = p.y + Mathf.Sin(i) * RY;
            Gizmos.DrawLine(new Vector3(lastx, lasty, 0), new Vector3(x, y, 0));
            lastx = x;
            lasty = y;
        }
        lastx = p.x + Mathf.Cos(StartPi) * RX;
        lasty = p.y + Mathf.Sin(StartPi) * RY;
        Gizmos.color = Color.green;
        Gizmos.DrawCube(new Vector3(lastx, lasty), new Vector3(0.5f, 0.5f, 1));
    }
}
