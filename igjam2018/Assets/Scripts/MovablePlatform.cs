﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlatform : MonoBehaviour {

    public float verticalMovement = 3.0f;
    public float speedMultiplier = 2.5f;
    public bool moveLeftOnCollision = true;

    private float yPos;
    private bool disappearing;
    private BoxCollider2D boxCollider;

    private void Awake() {
        yPos = transform.position.y;
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D coll) {
        if(moveLeftOnCollision && coll.gameObject.tag == "Player") {
            disappearing = true;
        }
    }

    void FixedUpdate () {
        if (verticalMovement > 0 && !disappearing) {
            transform.position = new Vector3(transform.position.x, yPos + Mathf.PingPong(Time.time * speedMultiplier, verticalMovement), transform.position.z);
        }
        if (disappearing) {
            transform.position += Vector3.left *  Time.deltaTime * speedMultiplier;
        }
    }
}
