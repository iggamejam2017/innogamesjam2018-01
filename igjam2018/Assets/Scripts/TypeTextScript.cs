﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeTextScript : MonoBehaviour {

    private string text;

    private Text TextUI;
    private int pos = 0;

    public GameObject FadeElement;
    public StartAnimationScript StartAnimScript;
    private AudioSource sfx;
    public AudioSource sent;


    // Use this for initialization
    void Start () {
        TextUI = GetComponent<Text>();
        text = TextUI.text;
        TextUI.text = "";
        sfx = GetComponent<AudioSource>();
        StartCoroutine(TypeText());
	}

    private IEnumerator TypeText()
    {

        while (++pos < text.Length)
        {
            if (text.ToCharArray()[pos] == ' ')
                pos++;

            TextUI.text = text.Substring(0, pos);
            sfx.Play();
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.1f));
        }

        TextUI.text = text;

        yield return new WaitForSeconds(2f);

        StartAnimScript.StartPlayer();
        sent.Play();

        var comps = FadeElement.GetComponentsInChildren<CanvasRenderer>();
        float a = 1.0f;
        while (a > 0f)
        {
            a -= Time.deltaTime;
            foreach (var c in comps)
            {
                c.SetAlpha(a);
            }
            yield return null;
        }

    }
}
