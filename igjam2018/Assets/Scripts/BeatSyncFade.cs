﻿using UnityEngine;
using System;

public class BeatSyncFade : MonoBehaviour
{
    private SpriteRenderer sprite;
    public Color FadeColor1;
    public Color FadeColor2;
    public float FadeSpeed = 0.1f;

    float t = 0;

    void Start()
    {
        //Select the instance of AudioProcessor and pass a reference
        //to this object
        AudioProcessor processor = FindObjectOfType<AudioProcessor>();
        //processor.onBeat.AddListener(onOnbeatDetected);
        processor.onSpectrum.AddListener(onSpectrum);

        sprite = GetComponent<SpriteRenderer>();
    }

    //this event will be called every time a beat is detected.
    //Change the threshold parameter in the inspector
    //to adjust the sensitivity
    public void onOnbeatDetected()
    {
        Debug.Log("Beat");
        sprite.color = FadeColor1;
        t = 0;
    }

    private void Update()
    {
        t += FadeSpeed * Time.deltaTime;       
        sprite.color = Color.Lerp(sprite.color, FadeColor2, t);
    }

    //This event will be called every frame while music is playing
    void onSpectrum(float[] spectrum)
    {
        //The spectrum is logarithmically averaged
        //to 12 bands

        for (int i = 0; i < spectrum.Length; ++i)
        {
            Vector3 start = new Vector3(i, 0, 0);
            Vector3 end = new Vector3(i, spectrum[i]*100, 0);
            Debug.DrawLine(start, start + new Vector3(1, 0, 0));
            Debug.DrawLine(start, end);
            Debug.DrawLine(start + new Vector3(1, 0, 0), end + new Vector3(1, 0, 0));
            Debug.DrawLine(end, end + new Vector3(1, 0, 0));
        }
    }
}