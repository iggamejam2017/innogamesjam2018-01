﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

    public float scrollMultiplier = 1;

    private Transform cameraTransform;

	// Use this for initialization
	void Start () {
        cameraTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        cameraTransform.position += Vector3.right * Time.deltaTime * scrollMultiplier;
	}
}
