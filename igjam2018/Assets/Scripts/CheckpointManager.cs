﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour {

    Vector3 LastCheckpoint;
    Checkpoint LastCP;
    private GameObject cam;
    private AudioSource music;
    public float RewindSpeed = 0.25f;

    bool resetting = false;


    // Use this for initialization
    void Start () {
        LastCheckpoint = transform.position;

        cam = GameObject.FindGameObjectWithTag("MainCamera");
        music = cam.GetComponent<AudioSource>();

        var FirstCP = new GameObject("FirstCheckpoint");
        FirstCP.transform.position = transform.position;

        LastCP = MakeCheckpoint(FirstCP); // new Checkpoint(cam.transform.position, 0f, FirstCP);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        //Debug.Log(coll.otherCollider.name);
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Trap" && !resetting)
        {
            //transform.position = LastCheckpoint;
            ResetToCheckpoint(LastCP);
        }
        else if (other.tag == "Checkpoint" && !resetting)
        {
            LastCP = MakeCheckpoint(other.gameObject);
            other.gameObject.SetActive(false);
            LastCheckpoint = transform.position;
        }
    }

    private Checkpoint MakeCheckpoint(GameObject other)
    {
        var c = new Checkpoint(cam.transform.position, music.time, other);

        var platforms = FindObjectsOfType<PlatformController>();
        foreach(var platform in platforms)
        {
            if (platform.OnlyOnce || true)
            {
                //Debug.Log(platform.gameObject.name);
                var ps = new PlatformState(platform);
                c.AddPS(ps);
            }
        }

        return c;
    }

    private void ResetToCheckpoint(Checkpoint cp)
    {
        resetting = true;
        StopAllCoroutines();
        StartCoroutine(Reset(cp));
    }

    private IEnumerator Reset(Checkpoint cp)
    {
        float diff = music.time - cp.musicTime;
        //music.pitch = -diff / RewindSpeed;
        float t = Time.time;
        while(Time.time - t < RewindSpeed)
        {
            float d = (Time.time - t) / RewindSpeed;
            cam.transform.position = Vector3.Lerp(cam.transform.position, cp.CameraPosition, d);
            transform.position = Vector3.Lerp(transform.position, cp.cpObject.transform.position, d);
            music.pitch = Mathf.Lerp(1, 0, d);
            yield return null;
        }

        //yield return new WaitForSeconds(RewindSpeed);
        
        foreach(var platform in cp.platformStates)
        {
            platform.Reset();
        }

        cam.transform.position = cp.CameraPosition;
        transform.position = cp.cpObject.transform.position;
        music.time = cp.musicTime;
        music.pitch = 1;
        resetting = false;
    }

    class Checkpoint
    {
        public Vector3 CameraPosition;
        public float musicTime;
        public GameObject cpObject;
        public List<PlatformState> platformStates;

        public Checkpoint(Vector3 camPos, float musicTime, GameObject cp)
        {
            this.CameraPosition = camPos;
            this.musicTime = musicTime;
            this.cpObject = cp;
            platformStates = new List<PlatformState>();
        }

        internal void AddPS(PlatformState ps)
        {
            platformStates.Add(ps);
        }
    }

    class PlatformState
    {
        public PlatformController p;
        public int fromWaypointIndex;
        public float percentBetweenWaypoints;
        public bool started;
        public bool stop;
        public Vector3 pos;

        public PlatformState(PlatformController p)
        {
            this.p = p;
            this.pos = p.transform.position;
            this.fromWaypointIndex = p.fromWaypointIndex;
            this.percentBetweenWaypoints = p.percentBetweenWaypoints;
            this.started = p.started;
            this.stop = p.stop;
        }

        internal void Reset()
        {
            p.fromWaypointIndex = fromWaypointIndex;
            p.percentBetweenWaypoints = percentBetweenWaypoints;
            p.started = started;
            p.stop = stop;
            p.transform.position = pos;
        }
    }
}
