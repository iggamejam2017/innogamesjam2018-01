﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFreqencyEvents : MonoBehaviour
{
    public AudioSource AS;
    public float threshold = 0.07f;
    private BeatSyncFade[] BeatingObjects;

    // Use this for initialization
    void Start()
    {
        if(AS == null)
            AS = GetComponent<AudioSource>();

        BeatingObjects = FindObjectsOfType<BeatSyncFade>();
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(BandVol(frqLow, frqHigh));

        DrawSpectrum();




        //float[] spectrum = new float[256];

        //AS.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);

        //for (int i = 1; i < spectrum.Length - 1; i++)
        //{
        //    Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);
        //    Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
        //    Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
        //    Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.blue);
        //}
    }

    bool lastWasBeat = false;
    private void DrawSpectrum()
    {
        int n = 256;
        float[] spectrum = new float[n];
        AS.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        float delta = 1.0f / n;

        bool beat = false;

        for (int i = 1; i < spectrum.Length - 1; i++)
        {
            Debug.DrawLine(new Vector3(i*delta, 0), new Vector3(i * delta, spectrum[i]));
            if(i >= 5 && i <= 15)
            {
                //Debug.Log(spectrum[6]);
                if (spectrum[i] >= threshold)
                    beat = true;
            }
        }

        if (beat)
        {
            if (!lastWasBeat)
            {
                Debug.Log("beat");
                lastWasBeat = true;
                foreach (var beatingObject in BeatingObjects)
                {
                    beatingObject.onOnbeatDetected();
                }
                //GetComponent<BeatSyncFade>().onOnbeatDetected();
            }
        }
        else
            lastWasBeat = false;
    }

    private float fMax = 24000;
    private float[] freqData = new float[256];
    private int nSamples = 256;
    public float frqLow = 700;
    public float frqHigh = 900;

    float BandVol(float fLow, float fHigh)
    {
        fLow = Mathf.Clamp(fLow, 20, fMax); // limit low...
        fHigh = Mathf.Clamp(fHigh, fLow, fMax); // and high frequencies
                                                // get spectrum: freqData[n] = vol of frequency n * fMax / nSamples
        AS.GetSpectrumData(freqData, 0, FFTWindow.BlackmanHarris);
        int n1 = (int)Mathf.Floor(fLow * nSamples / fMax);
        int n2 = (int)Mathf.Floor(fHigh * nSamples / fMax);
        float sum = 0;

        // average the volumes of frequencies fLow to fHigh
        for (var i = n1; i <= n2; i++)
        {
            sum += freqData[i];
        }

        return sum / (n2 - n1 + 1);
    }
}
