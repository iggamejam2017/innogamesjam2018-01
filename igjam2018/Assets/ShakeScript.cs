﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeScript : MonoBehaviour {


    public void Shake()
    {
        StartCoroutine(ShakingPlatforms());
    }

    private IEnumerator ShakingPlatforms()
    {
        float starttime = Time.time;
        float shakeAmount = 50f;
        float startDuration = 5f;
        float shakeDuration = startDuration;
        float shakePercentage = 0f;
        float startAmount = 10f;
        float smoothAmount = 5f;
        bool smooth = false;

        // http://wiki.unity3d.com/index.php/Camera_Shake

        while (shakeDuration > 0.01f)
        {
            Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;//A Vector3 to add to the Local Rotation
            rotationAmount.x = 0;//Don't change the Z; it looks funny.
            rotationAmount.y = 0;
            shakePercentage = shakeDuration / startDuration;//Used to set the amount of shake (% * startAmount).

            shakeAmount = startAmount * shakePercentage;//Set the amount of shake (% * startAmount).
            shakeDuration = Mathf.Lerp(shakeDuration, 0, Time.deltaTime);//Lerp the time, so it is less and tapers off towards the end.
            //Debug.Log(rotationAmount);

            if (smooth)
            {
                transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(rotationAmount), Time.deltaTime * smoothAmount);
            }
            else
            {
                transform.localRotation = Quaternion.Euler(rotationAmount);//Set the local rotation the be the rotation amount.
                //platforms[0].transform.localRotation = Quaternion.Euler(rotationAmount);
            }

            yield return null;
        }

        yield return null;
    }
}
