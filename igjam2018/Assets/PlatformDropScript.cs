﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;

using UnityEngine.SceneManagement;

public class PlatformDropScript : MonoBehaviour
{

    public GameObject[] Disables; // Disable
    public GameObject[] platforms;
    public GameObject MainCamera;

    private FadeScript fade;
    private AudioSource music;
    private bool Crashed = false;

    // Use this for initialization
	void Start ()
	{
	    music = MainCamera.GetComponent<AudioSource>();
	    fade = GetComponent<FadeScript>();

        // Testing
	    //music.time = 45f;
	    //StartCrash();
	}
	
	// Update is called once per frame
	void Update () {
	    if (music.time >= 48f)
	    {
	        if (!Crashed)
	        {
	            StartCrash();
	        }
	    }
	}

    private void StartCrash()
    {
        Crashed = true;

        foreach (var platform in platforms)
        {
            var s = platform.AddComponent<ShakeScript>();
            s.Shake();
        }

        StartCoroutine(DropAndEnd());
    }

    private IEnumerator DropAndEnd()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (var d in Disables)
        {
            d.SetActive(false);
        }
        foreach (var platform in platforms)
        {
            Destroy(platform.GetComponent<BoxCollider2D>());
            var rb = platform.AddComponent<Rigidbody2D>();
            rb.gravityScale = 3f;
            rb.AddForce(new Vector2(-1, 1) * 1000f);
        }

        yield return new WaitForSeconds(0.5f);

        fade.fade = true;

        while (music.isPlaying)
        {
            yield return null;
        }

        SceneManager.LoadScene("StartScreen");
    }

    
}
