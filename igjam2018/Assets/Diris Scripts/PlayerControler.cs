﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour {
		
		public float movementSpeed = 365f;
		public float maxSpeed = 5f;
		public float jumpSpeed = 1000f;
		private Rigidbody2D player;
		public bool jumpable = true;
		public bool jumping = false;
		public float fallMultiplier = 2.5f;
		public float lowJumpFallMultiplier = 2f;
		public bool facingRight = true;
		Animator anim;



	// Use this for initialization
	void Start () {
		player = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {


		//Left/Right Movement
		float horizontalMovement = Input.GetAxisRaw("Horizontal");
		//Vector2 movement = new Vector2 (horizontalMovement, 0);
		if (horizontalMovement * player.velocity.x < maxSpeed)
		{
			player.velocity = new Vector2 (horizontalMovement * maxSpeed, player.velocity.y);
		}

		if (Mathf.Abs(player.velocity.x) > maxSpeed)
		{
			player.velocity = new Vector2(Mathf.Sign(player.velocity.x) *maxSpeed, player.velocity.y);

		}
		anim.SetFloat("Speed", Mathf.Abs(horizontalMovement));

		if (horizontalMovement > 0 && !facingRight)
		{
			Flip();
		} else if (horizontalMovement < 0 && facingRight)
		{
			Flip();
		}
		//Jumping
		if (Input.GetButtonDown("Jump") && jumpable)
		{
			jumping = true;	
			jumpable = false;

		}
		if (jumping)
		{
			player.AddForce(new Vector2(0f, jumpSpeed));
			jumping = false;
		}
		if (player.velocity.y < 0)
		{
			player.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier -1) * Time.deltaTime;
		}else if(player.velocity.y > 0 && !Input.GetButton("Jump"))
		{
			player.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpFallMultiplier - 1) * Time.deltaTime;
		}
		if (horizontalMovement == 0)
		{
			player.velocity = new Vector2(0, player.velocity.y);
		}
		//	transform.Translate(movement * movementSpeed);
		//	if (Input.GetButtonDown("Jump") && jumpable)
		//	{
		//		Vector2 jumpMove = new Vector2 (0, 1);
		//		transform.Translate(jumpMove * jumpSpeed);
		//	}
		

		
	}
	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "ground")
		{
			jumpable = true;
		}
		if (coll.gameObject.tag == "wall")
		{
			jumpable = true;

		}
	}

	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
