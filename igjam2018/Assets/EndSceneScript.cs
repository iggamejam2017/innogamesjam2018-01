﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSceneScript : MonoBehaviour {

    public AudioSource sent;
    public AudioSource received;
    public AudioSource transition;

    public GameObject Message;

    public string nextScene;

    bool ended = false;

    void Start()
    {
        var comps = Message.GetComponentsInChildren<CanvasRenderer>();
        foreach(var c in comps)
        {
            c.SetAlpha(0f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ended = true;
            StartCoroutine(EndScene());
        }
    }

    private IEnumerator EndScene()
    {
        received.Play();
        transition.Play();

        var comps = Message.GetComponentsInChildren<CanvasRenderer>();
        float a = 0f;
        while (a < 1f)
        {
            a += Time.deltaTime;
            foreach (var c in comps)
            {
                c.SetAlpha(a);
            }
            yield return null;
        }

        yield return new WaitForSeconds(9f);
        
        SceneManager.LoadScene(nextScene);
    }
}
