﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationScript : MonoBehaviour {

    public GameObject PlayerAnimator;

	// Use this for initialization
	void Start () {
        //var anim = GetComponent<Animator>();
        
	}

    private IEnumerator ActivatePlayer()
    {
        yield return new WaitForSeconds(2.1f);
        PlayerAnimator.SetActive(true);
        transform.parent.GetComponent<PlayerInput>().enabled = true;
        transform.parent.parent = null;
        gameObject.SetActive(false);


    }

    internal void StartPlayer()
    {
        var anim = GetComponent<Animator>();
        anim.SetTrigger("Start");
        StartCoroutine(ActivatePlayer());
    }
}
